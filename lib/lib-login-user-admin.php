<?php
class Admin{
  /* [DATABASE HELPER FUNCTIONS] */
  private $pdo = null;
  private $stmt = null;
  public $error = "";

  function __construct(){
    try {
      $this->pdo = new PDO(
        "mysql:host=".DB_HOST.";dbname=".DB_DB.";charset=".DB_CHAR,
        DB_USER, DB_PASS, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false,
        ]
      );
    } catch (Exception $ex) { die($ex->getMessage()); }
  }

  function __destruct(){
    if ($this->stmt!==null) { $this->stmt = null; }
    if ($this->pdo!==null) { $this->pdo = null; }
  }

  function query($sql, $cond=[]){
    try {
      $this->stmt = $this->pdo->prepare($sql);
      $this->stmt->execute($cond);
    } catch (Exception $ex) { 
      $this->error = $ex->getMessage();
      return false;
    }
    $this->stmt = null;
    return true;
  }

  function query_otra($sql, $cond=[]){
    try {

      $this->stmt = $this->pdo->prepare($sql);
      $this->stmt->execute($cond);  
      
    } catch (Exception $ex) { 
      $this->error = $ex->getMessage();
      return false;
    }
    return $this->pdo->lastInsertId();
  }

  /* [USER FUNCTIONS] */
  function getAll(){
    $this->stmt = $this->pdo->prepare("SELECT * FROM `users`");
    $this->stmt->execute();
    $users = $this->stmt->fetchAll();
	return count($users)==0 ? false : $users;
  }

  function getEmail($email){
    $this->stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE `email`=?");
	$cond = [$email];
    $this->stmt->execute($cond);
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user[0];
  }

  function getID($id){
    $this->stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE `id`=?");
	$cond = [$id];
    $this->stmt->execute($cond);
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user[0];
  }

  function create($name, $email, $password){
    return $this->query(
      "INSERT INTO `users` (`name`, `email`, `password`) VALUES (?,?,?)",
      [$name, $email, openssl_encrypt($password, "AES-128-ECB", SECRET_KEY)]
    );
  }

  function update($name, $email, $password="", $id){
	$q = "UPDATE `users` SET `name`=?, `email`=?";
	$cond = [$name, $email];
	if ($password!="") {
		$q .= ", `password`=?";
		$cond[] = openssl_encrypt($password, "AES-128-ECB", SECRET_KEY);
	}
	$q .= " WHERE `id`=?";
	$cond[] = $id;
    return $this->query($q, $cond);
  }

  function delete($id){
    return $this->query(
      "DELETE FROM `users` WHERE `id`=?",
      [$id]
    );
  }

  function login($email, $password){
    $this->stmt = $this->pdo->prepare("SELECT id_adm_s, nombre_adm_s, apellido_adm_s, email_adm_s, perfil_adm_s, valor FROM ADM_SITE AS A_S, PERFILES_USUARIO AS PU 
    WHERE A_S.usuario_adm_s=? AND A_S.pass_adm_s=? AND A_S.perfil_adm_s = PU.id_p_u");
    $cond = [$email, $password];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user[0];
  }
  
  function valoresInicio($estado = ""){
	  if($estado != ""){
		  $this->stmt = $this->pdo->prepare("SELECT * FROM VALORES_PAGINA_INICIO WHERE status = '1'");	
	  }else{
		  $this->stmt = $this->pdo->prepare("SELECT * FROM VALORES_PAGINA_INICIO");	
	  }
	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function valoresInicioporId($id){
	$this->stmt = $this->pdo->prepare("SELECT * FROM VALORES_PAGINA_INICIO WHERE id_v_p_i=?");
	$cond = [$id];
    $this->stmt->execute($cond);
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function valoresPaisesAll($id){
    $this->stmt = $this->pdo->prepare("SELECT * FROM VALORES_PAGINA_INICIO WHERE id_v_p_i=?");
    $cond = [$id];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
    }
  function createValoresInicio($campo, $valor_es, $valor_en, $valor_ch){
    return $this->query(
      "INSERT INTO VALORES_PAGINA_INICIO (id_v_p_i, campo, valor_es, valor_en, valor_ch, status, created, modified) VALUES ('', ?, ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
      [$campo, $valor_es, $valor_en, $valor_ch]
    );
  }
  function updateCampoTabla($tabla, $campo, $valor_campo, $campo_id, $id){
	$q = "UPDATE ".$tabla." SET ".$campo."= '".$valor_campo."'";
	if ($campo_id != "" && $id != "") {
		$q .= " WHERE ".$campo_id."= '".$id."'";
	}
	$respuesta = $this->query($q);	  
    return $respuesta;
  }
  function valoresPaises($start_from, $record_per_page, $buscar_por_codigo, $buscar_por_pais){
	$condicion = "";  
	if($buscar_por_codigo != ""){
		$condicion .= " AND codigo_pais LIKE '%".$buscar_por_codigo."%'";
	}
	if($buscar_por_pais != ""){
		$condicion .= " AND pais LIKE '%".$buscar_por_pais."%'";
	}
	$this->stmt = $this->pdo->prepare("SELECT * FROM PAISES WHERE 1".$condicion." ORDER BY codigo_pais ASC LIMIT ".$start_from."," . $record_per_page);
	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function contarvaloresPaises($buscar_por_codigo, $buscar_por_pais){
	$condicion = "";
	if($buscar_por_codigo != ""){
		$condicion .= " AND codigo_pais LIKE '%".$buscar_por_codigo."%'";
	}
	if($buscar_por_pais != ""){
		$condicion .= " AND pais LIKE '%".$buscar_por_pais."%'";
	}
	$this->stmt = $this->pdo->prepare("SELECT * FROM PAISES WHERE 1".$condicion." ORDER BY codigo_pais ASC");
	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : count($user);
  }
  function valoresCiudades($start_from, $record_per_page, $buscar_por_codigo, $buscar_por_ciudad){
	$condicion = "";  
	if($buscar_por_codigo != ""){
		$condicion .= " AND CI.paises_codigo LIKE '%".$buscar_por_codigo."%'";
	}
	if($buscar_por_ciudad != ""){
		$condicion .= " AND CI.ciudad LIKE '%".$buscar_por_ciudad."%'";
	}
	$this->stmt = $this->pdo->prepare("SELECT * FROM CIUDADES AS CI 
                                    LEFT JOIN PAISES P on CI.paises_codigo = P.codigo_pais 
                                    WHERE 1".$condicion." ORDER BY CI.paises_codigo 
                                    ASC LIMIT ".$start_from."," . $record_per_page);
	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
	//return [$start_from, $record_per_page, $buscar_por_codigo, $buscar_por_ciudad];
  }
  function contarvaloresCiudades($buscar_por_codigo, $buscar_por_ciudad){
	$condicion = "";  
	if($buscar_por_codigo != ""){
		$condicion .= " AND paises_codigo LIKE '%".$buscar_por_codigo."%'";
	}
	if($buscar_por_ciudad != ""){
		$condicion .= " AND ciudad LIKE '%".$buscar_por_ciudad."%'";
	}	
	return ["SELECT * FROM CIUDADES WHERE 1".$condicion." ORDER BY paises_codigo ASC"];
  }
  function valoresCategorias(){	
	$this->stmt = $this->pdo->prepare("SELECT CN.id_c_n, CN.id_c_p, CNN.valor_es AS categoria, CN.valor_es, CN.valor_en, CN.valor_ch, CN.description_es, CN.description_en, CN.description_ch, CN.keywords_es, CN.keywords_en, CN.keywords_ch, CN.status, CN.created, CN.modified 
	FROM CATEGORIAS_NOTICIAS AS CN 
	LEFT JOIN CATEGORIAS_NOTICIAS CNN on CN.id_c_p = CNN.id_c_n
	ORDER BY CN.id_c_n ASC");	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function deleteCampoTabla($tabla, $campo_id, $id){
	$q = "DELETE FROM ".$tabla." WHERE ".$campo_id."= '".$id."'";
	$respuesta = $this->query($q);	  
    return $respuesta;
  }
  function createCategoriaNoticias($categoria, $valor_es, $valor_en, $valor_ch, $description_es, $description_en, $description_ch, $keywords_es, $keywords_en, $keywords_ch){
    return $this->query(
      "INSERT INTO CATEGORIAS_NOTICIAS (id_c_n, id_c_p, valor_es, valor_en, valor_ch, description_es, description_en, description_ch, keywords_es, keywords_en, keywords_ch, status, created, modified) VALUES ('', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
      [$categoria, $valor_es, $valor_en, $valor_ch, $description_es, $description_en, $description_ch, $keywords_es, $keywords_en, $keywords_ch]
    );
  }
  function valoresCategoriasNoticiasporId($id){
	$this->stmt = $this->pdo->prepare("SELECT * FROM CATEGORIAS_NOTICIAS WHERE id_c_n=?");
	$cond = [$id];
    $this->stmt->execute($cond);
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function valoresCategoriasMenuInicioRandom(){	
	$this->stmt = $this->pdo->prepare("SELECT CN.id_c_n, CN.id_c_p, CNN.valor_es AS categoria, CN.valor_es, CN.valor_en, CN.valor_ch, CN.description_es, CN.description_en, CN.description_ch, CN.keywords_es, CN.keywords_en, CN.keywords_ch, CN.status, CN.created, CN.modified 
	FROM CATEGORIAS_NOTICIAS AS CN 
	LEFT JOIN CATEGORIAS_NOTICIAS CNN on CN.id_c_p = CNN.id_c_n
  WHERE CN.STATUS = '1'
	ORDER BY RAND() ASC LIMIT 4");	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function valoresAutoresNoticias(){	
	$this->stmt = $this->pdo->prepare("SELECT id_a_n, nombre_autor, img_autor, acerca_autor_es, acerca_autor_en, acerca_autor_ch, link_fb_a_n, link_tw_a_n, link_inst_a_n, status, created, modified 
	FROM AUTORES_NOTICIAS
	ORDER BY id_a_n ASC");	
    $this->stmt->execute();
    $user = $this->stmt->fetchAll();
	return count($user)==0 ? false : $user;
  }
  function createAutoresNoticias($nombre_autor, $acerca_autor_es, $acerca_autor_en, $acerca_autor_ch, $link_fb_a_n, $link_tw_a_n, $link_inst_a_n, $img_autor){
    return $this->query(
      "INSERT INTO AUTORES_NOTICIAS (id_a_n, nombre_autor, img_autor, acerca_autor_es, acerca_autor_en, acerca_autor_ch, link_fb_a_n, link_tw_a_n, link_inst_a_n, status, created, modified) VALUES ('', ?, ?, ?, ?, ?, ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
      [$nombre_autor, $img_autor, $acerca_autor_es, $acerca_autor_en, $acerca_autor_ch, $link_fb_a_n, $link_tw_a_n, $link_inst_a_n]
    );
  }
  function valoresAutoresNoticiasporId($id){
    $this->stmt = $this->pdo->prepare("SELECT * FROM AUTORES_NOTICIAS WHERE id_a_n=?");
    $cond = [$id];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  function valoresNoticias(){	
    $this->stmt = $this->pdo->prepare("SELECT N.id_n, N.titulo_corto_n_es, N.titulo_corto_n_en, N.titulo_corto_n_ch, N.titulo_largo_n_es, N.titulo_largo_n_en, N.titulo_largo_n_ch, N.autor_n, N.visitas_n, N.fecha_publi_n, N.status, N.created, N.modified
    FROM NOTICIAS AS N 
    WHERE 1");	
      $this->stmt->execute();
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  function createNoticia($titulo_corto_n_es, $titulo_corto_n_en, $titulo_corto_n_ch, $titulo_largo_n_es, $titulo_largo_n_en, $titulo_largo_n_ch, $autor_n){
      return $this->query_otra(
        "INSERT INTO NOTICIAS (id_n, titulo_corto_n_es, titulo_corto_n_en, titulo_corto_n_ch, titulo_largo_n_es, titulo_largo_n_en, titulo_largo_n_ch, autor_n, visitas_n, fecha_publi_n, status, created, modified) VALUES ('', ?, ?, ?, ?, ?, ?, ?, '0', '', 0, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$titulo_corto_n_es, $titulo_corto_n_en, $titulo_corto_n_ch, $titulo_largo_n_es, $titulo_largo_n_en, $titulo_largo_n_ch, $autor_n]
      );
  }
  function noticiasCategorias($id_n){	
    $this->stmt = $this->pdo->prepare("SELECT * FROM NOTICIAS_CATEGORIAS AS NC LEFT JOIN CATEGORIAS_NOTICIAS CNN on NC.id_c_n = CNN.id_c_n WHERE NC.id_n='".$id_n."'");	
      $this->stmt->execute();
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  function createNoticiasCategorias($id_n, $id_c_n){
      return $this->query_otra(
        "INSERT INTO NOTICIAS_CATEGORIAS (id_n_c, id_n, id_c_n, status, created, modified) VALUES ('', ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$id_n, $id_c_n]
      );
  }
  function valoresImagenesNoticiasporId($id){
    $this->stmt = $this->pdo->prepare("SELECT * FROM NOTICIAS_IMG WHERE id_n=?");
    $cond = [$id];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  function createImagenNoticias($id_n, $tipo_img_n, $archivo_img){
      return $this->query_otra(
        "INSERT INTO NOTICIAS_IMG (id_n_i, id_n, tipo_img_n, archivo_img, status, created, modified) VALUES ('', ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$id_n, $tipo_img_n, $archivo_img]
      );
  }
  function createdescripcionCortaNoticias($id_n, $descripcion_corta_es, $descripcion_corta_en, $descripcion_corta_ch){
      return $this->query_otra(
        "INSERT INTO NOTICIAS_DESCRIP_CORTA (id_n_d_c, id_n, descripcion_corta_es, descripcion_corta_en, descripcion_corta_ch, status, created, modified) VALUES ('', ?, ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$id_n, $descripcion_corta_es, $descripcion_corta_en, $descripcion_corta_ch]
      );
  }
  function createdescripcionLargaNoticias($id_n, $descripcion_larga_es, $descripcion_larga_en, $descripcion_larga_ch){
      return $this->query_otra(
        "INSERT INTO NOTICIAS_DESCRIP_LARGA (id_n_d_l, id_n, descripcion_larga_es, descripcion_larga_en, descripcion_larga_ch, status, created, modified) VALUES ('', ?, ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$id_n, $descripcion_larga_es, $descripcion_larga_en, $descripcion_larga_ch]
      );
  }
  function crearNoticiasPaises($id_n, $codigo_pais){
      return $this->query_otra(
        "INSERT INTO PAIS_NOTICIAS (id_p_n, id_n, codigo_pais, status, created, modified) VALUES ('', ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$id_n, $codigo_pais]
      );
  }
  function noticiasPaises($id_n){	
    $this->stmt = $this->pdo->prepare("SELECT * FROM PAIS_NOTICIAS AS PN LEFT JOIN PAISES P on PN.codigo_pais = P.codigo_pais WHERE PN.id_n='".$id_n."'");	
      $this->stmt->execute();
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  function crearNoticiasTags($id_n, $tags_noticias_es, $tags_noticias_en, $tags_noticias_ch){
      return $this->query_otra(
        "INSERT INTO TAGS_NOTICIAS (id_t_n, id_n, texto_t_n_es, texto_t_n_en, texto_t_n_ch, status, created, modified) VALUES ('', ?, ?, ?, ?, 1, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')",
        [$id_n, $tags_noticias_es, $tags_noticias_en, $tags_noticias_ch]
      );
  }
  function consultarNoticiaId($id_n){
    $this->stmt = $this->pdo->prepare("SELECT * FROM NOTICIAS WHERE id_n=?");
    $cond = [$id_n];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  function consultarDescripcionNoticiasId($id_n){
    $this->stmt = $this->pdo->prepare("SELECT * FROM NOTICIAS_DESCRIP_CORTA WHERE id_n=?");
    $cond = [$id_n];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  
  function consultarDescripcionLargaNoticiasId($id_n){
    $this->stmt = $this->pdo->prepare("SELECT * FROM NOTICIAS_DESCRIP_LARGA WHERE id_n=?");
    $cond = [$id_n];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
  
  function consultarTagsNoticiasId($id_n){
    $this->stmt = $this->pdo->prepare("SELECT * FROM TAGS_NOTICIAS WHERE id_n=?");
    $cond = [$id_n];
      $this->stmt->execute($cond);
      $user = $this->stmt->fetchAll();
    return count($user)==0 ? false : $user;
  }
}
?>