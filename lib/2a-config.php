<?php
// MUTE NOTICES
error_reporting(E_ALL & ~E_NOTICE);

// DATABASE SETTINGS
define("DB_HOST", "mysql.npm24.com");
define("DB_DB", "npm24");
define("DB_CHAR", "utf8");
define("DB_USER", "userdbnpm24");
define("DB_PASS", "Us3RDBNpm24");


define("AUTHORIZATION_ADMIN", "NpM24AuThOrIzAtIoNAdMiN");
define("AUTHORIZATION_PORTAL", "NpM24AuThOrIzAtIoNPoRtAl");
define("REFER_PORTAL", "NpM24ReFerPoRtAlAdMiN");



// FILE PATH
// Manually define the absolute path if you get path problems
define('PATH_LIB', __DIR__ . DIRECTORY_SEPARATOR);

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'NpM24KeY';
    $secret_iv = 'NpM24Iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

?>