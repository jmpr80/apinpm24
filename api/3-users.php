<?php
//print_r($_GET); die;
$headers = apache_request_headers();


header('Content-Type: application/json');
if(isset($_REQUEST)){
	$username = $_REQUEST['username'];
	$pass = $_REQUEST['pass'];
}

$body = json_decode(file_get_contents("php://input"), true);
print_r($body);
if(isset($body['username']) && isset($body['pass'])){
	$body = json_decode(file_get_contents("php://input"), true);
	$username = $body['username'];
	$pass = $body['pass'];
}

/*$body = json_decode(file_get_contents("php://input"), true);
$username = $body['username'];
$pass = $body['pass'];*/

if (isset($_GET['metodo'])) {
  // INIT
  require dirname(__DIR__) . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "2a-config.php";
  require PATH_LIB . "2b-lib-user.php";
  $users = new User();

  // PROCESS REQUEST
  switch ($_GET['metodo']) {
    default:
      echo json_encode([
        "status" => false,
        "message" => "Invalid Request"
      ]);
      break;

    case "get-all":
    $all = $users->getAll();
	if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0){
      echo json_encode([
        "status" => true,
        "data" => $all,
		"username" => $username,
		"pass" => $pass,
		"Authorization" => $headers['Authorization'],
		"message" => "consulta realizada con exito"
      ]);
	}else{
		echo json_encode([
        "status" => false,
        "data" => null,
		"message" => "Error consultando informacion"
      ]);
	}
      break;

    case "get-email":
    $usr = $users->getEmail($_REQUEST["email"]);
      echo json_encode([
        "status" => $all==false?false:true,
        "data" => $usr
      ]);
      break;

    case "get-id":
    $usr = $users->getID($_POST['id']);
      echo json_encode([
        "status" => $all==false?false:true,
        "data" => $usr
      ]);
      break;

    case "create":
      $usr = $users->getEmail($_POST['email']);
      if (is_array($usr)) {
        echo json_encode([
          "status" => 0,
          "message" => $_POST['email'] . " already exist"
        ]);
      } else {
        $pass = $users->create($_POST['name'], $_POST['email'], $_POST['password']);
        echo json_encode([
          "status" => $pass,
          "message" => $pass ? "User Created" : "Error creating user"
        ]);
      }
      break;

    case "update":
      $pass = $users->update($_POST['name'], $_POST['email'], $_POST['password'], $_POST['id']);
      echo json_encode([
        "status" => $pass,
        "message" => $pass ? "User Updated" : "Error updating user"
      ]);
      break;

    case "delete":
      $pass = $users->delete($_POST['id']);
      echo json_encode([
        "status" => $pass,
        "message" => $pass ? "User Deleted" : "Error deleting user"
      ]);
      break;

    case "login":
      if (is_array($_SESSION['user'])) {
        die(json_encode([
              "status" => true,
              "message" => "Already signed in"
          ]));
      }
        $pass = $users->login($_POST['name'], $_POST['password']);
        if ($pass!==false) { $_SESSION['user'] = $pass; }
      echo json_encode([
          "status" => is_array($pass),
          "message" => is_array($pass) ? "OK" : "Error"
      ]);
      break;

    case "logoff":
      unset($_SESSION['user']);
      echo json_encode([
          "status" => true,
          "message" => "OK"
      ]);
      break;
  }
}
?>