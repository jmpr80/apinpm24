<?php
//print_r($_GET); die;
$headers = apache_request_headers();
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

if (isset($_GET['metodo'])) {
  // INIT
  require dirname(__DIR__) . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "2a-config.php";  
  require PATH_LIB . "lib-login-user-admin.php";
  
  $admin = new Admin();
  
  // PROCESS REQUEST
  switch ($_GET['metodo']) {
    default:
      echo json_encode([
        "status" => false,
        "message" => "Invalid Request"
      ]);
      break;
	  
//LogIn administración
    case "login-admin":
		if(isset($_REQUEST)){
			$username = $_REQUEST['username'];
			$pass = $_REQUEST['pass'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['username']) && isset($body['pass'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$username = $body['username'];
			$pass = $body['pass'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->login($username, encrypt_decrypt("encrypt", $pass));
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Autenticacion Exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en la Autenticacion, usuario o contraseña incorrectos",				
				"data" => null
			  ]);
			}				
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
      break;
	  //Sacar valores-inicio
	  case "valores-inicio":
	  
		$estado = "";
		if(isset($_REQUEST)){
			$estado = $_REQUEST['estado'];
		}

		$body = json_decode(file_get_contents("php://input"), true);
		
		if(isset($body['estado'])){
			$estado = $body['estado'];
		}
			
		
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresInicio($estado);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de valores de inicio exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de valores de inicio",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar valores-inicio
	  case "crear-valores-inicio":
		if(isset($_REQUEST)){
			$campo = $_REQUEST['campo'];
			$valor_es = $_REQUEST['valor_es'];
			$valor_en = $_REQUEST['valor_en'];
			$valor_ch = $_REQUEST['valor_ch'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['campo']) && isset($body['valor_es']) && isset($body['valor_en']) && isset($body['valor_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$campo = $body['campo'];
			$valor_es = $body['valor_es'];
			$valor_en = $body['valor_en'];
			$valor_ch = $body['valor_ch'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createValoresInicio($campo, $valor_es, $valor_en, $valor_ch);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Insercion de valores pagina de inicio exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error insertando valores de inicio",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Sacar valores-inicio por Id
	  case "valores-inicio-id":
		if(isset($_REQUEST)){
			$id_v_p_i = $_REQUEST['id_v_p_i'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_v_p_i'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_v_p_i = $body['id_v_p_i'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresInicioporId($id_v_p_i);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de valores de inicio exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de valores de inicio",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  
	  //Sacar valores-inicio por Id
	  case "update-campo-tabla":
		if(isset($_REQUEST)){
			$tabla = $_REQUEST['tabla'];
			$campo = $_REQUEST['campo'];
			$valor_campo = $_REQUEST['valor_campo'];
			$campo_id = $_REQUEST['campo_id'];
			$id = $_REQUEST['id'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['tabla'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$tabla = $body['tabla'];
			$campo = $body['campo'];
			$valor_campo = $body['valor_campo'];
			$campo_id = $body['campo_id'];
			$id = $body['id'];
		}
		
		if((isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN) && (isset($headers['Referer']) && strlen($headers['Referer']) > 0 && $headers['Referer'] == REFER_PORTAL)){
			$respuesta = $admin->updateCampoTabla($tabla, $campo, $valor_campo, $campo_id, $id);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Cambio de valor exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en cambio de valores de inicio",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}	  
	  break;
	  //Sacar valores-paises
	  case "valores-paises":
		$start_from = "";
		$record_per_page = "";
		$buscar_por_codigo = "";
		$buscar_por_pais = "";
		if(isset($_REQUEST)){
			$start_from = $_REQUEST['start_from'];
			$record_per_page = $_REQUEST['record_per_page'];
			$buscar_por_codigo = $_REQUEST['buscar_por_codigo'];
			$buscar_por_pais = $_REQUEST['buscar_por_pais'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['start_from'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$start_from = $body['start_from'];
			$record_per_page = $body['record_per_page'];
			$buscar_por_codigo = $body['buscar_por_codigo'];
			$buscar_por_pais = $body['buscar_por_pais'];
		}
		
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresPaises($start_from, $record_per_page, $buscar_por_codigo, $buscar_por_pais);
			$respuesta1 = $admin->contarvaloresPaises($buscar_por_codigo, $buscar_por_pais);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Paises exitosa",
						"cuenta" => $respuesta1,
						"data" => $respuesta						
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Paises",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Sacar valores-paises-all
	  case "valores-paises-all":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresPaisesAll($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de valores de inicio exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de valores de inicio",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Sacar valores-ciudades
	  case "valores-ciudades":
		$start_from = "";
		$record_per_page = "";
		$buscar_por_codigo = "";
		$buscar_por_ciudad = "";
		if(isset($_REQUEST)){
			$start_from = $_REQUEST['start_from'];
			$record_per_page = $_REQUEST['record_per_page'];
			$buscar_por_codigo = $_REQUEST['buscar_por_codigo'];
			$buscar_por_ciudad = $_REQUEST['buscar_por_ciudad'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body)){
			$body = json_decode(file_get_contents("php://input"), true);
			$start_from = $body['start_from'];
			$record_per_page = $body['record_per_page'];
			$buscar_por_codigo = $body['buscar_por_codigo'];
			$buscar_por_ciudad = $body['buscar_por_ciudad'];
		}
		
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			
			$respuesta = $admin->valoresCiudades($start_from, $record_per_page, $buscar_por_codigo, $buscar_por_ciudad);
			
			$respuesta1 = $admin->contarvaloresCiudades($buscar_por_codigo, $buscar_por_ciudad);
			
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Ciudades exitosa",
						"cuenta" => $respuesta1,
						"data" => $respuesta						
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Ciudades",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  case "categorias-noticias":
		
		
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			
			$respuesta = $admin->valoresCategorias();
			
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Categorias exitosa",
						"data" => $respuesta						
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Categorias",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //delete-campo-tabla
	  case "delete-campo-tabla":
		if(isset($_REQUEST)){
			$tabla = $_REQUEST['tabla'];
			$campo_id = $_REQUEST['campo_id'];
			$id = $_REQUEST['id'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['tabla'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$tabla = $body['tabla'];
			$campo_id = $body['campo_id'];
			$id = $body['id'];
		}
		
		if((isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN) && (isset($headers['Referer']) && strlen($headers['Referer']) > 0 && $headers['Referer'] == REFER_PORTAL)){
			$respuesta = $admin->deleteCampoTabla($tabla, $campo_id, $id);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Registro Eliminado Exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Eliminando Registro",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-categoria-noticias
	  case "crear-categoria-noticias":
		if(isset($_REQUEST)){
			$categoria = $_REQUEST['categoria'];
			$valor_es = $_REQUEST['valor_es'];
			$valor_en = $_REQUEST['valor_en'];
			$valor_ch = $_REQUEST['valor_ch'];
			$description_es = $_REQUEST['description_es'];
			$description_en = $_REQUEST['description_en'];
			$description_ch = $_REQUEST['description_ch'];
			$keywords_es = $_REQUEST['keywords_es'];
			$keywords_en = $_REQUEST['keywords_en'];
			$keywords_ch = $_REQUEST['keywords_ch'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['categoria']) && isset($body['valor_es']) && isset($body['valor_en']) && isset($body['valor_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$categoria = $body['categoria'];
			$valor_es = $body['valor_es'];
			$valor_en = $body['valor_en'];
			$valor_ch = $body['valor_ch'];
			$description_es = $body['description_es'];
			$description_en = $body['description_en'];
			$description_ch = $body['description_ch'];
			$keywords_es = $body['keywords_es'];
			$keywords_en = $body['keywords_en'];
			$keywords_ch = $body['keywords_ch'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createCategoriaNoticias($categoria, $valor_es, $valor_en, $valor_ch, $description_es, $description_en, $description_ch, $keywords_es, $keywords_en, $keywords_ch);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Insercion de Categoria exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error insertando Categoria",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Sacar valores-categorias-noticias por Id
	  case "valores-categorias-noticias-id":
		if(isset($_REQUEST)){
			$id_c_n = $_REQUEST['id_c_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_c_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_c_n = $body['id_c_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresCategoriasNoticiasporId($id_c_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de valores de inicio exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de valores de inicio",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  case "categorias-noticias-menu-inicio-random":
		
		
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			
			$respuesta = $admin->valoresCategoriasMenuInicioRandom();
			
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Categorias exitosa",
						"data" => $respuesta						
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Categorias",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  case "autores-noticias":
		
		
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			
			$respuesta = $admin->valoresAutoresNoticias();
			
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Autores exitosa",
						"data" => $respuesta						
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Autores",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-autores-noticias
	  case "crear-autores-noticias":
		if(isset($_REQUEST)){
			$nombre_autor = $_REQUEST['nombre_autor'];
			$acerca_autor_es = $_REQUEST['acerca_autor_es'];
			$acerca_autor_en = $_REQUEST['acerca_autor_en'];
			$acerca_autor_ch = $_REQUEST['acerca_autor_ch'];
			$link_fb_a_n = $_REQUEST['link_fb_a_n'];
			$link_tw_a_n = $_REQUEST['link_tw_a_n'];
			$link_inst_a_n = $_REQUEST['link_inst_a_n'];
			$img_autor = $_REQUEST['img_autor'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['categoria']) && isset($body['valor_es']) && isset($body['valor_en']) && isset($body['valor_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$nombre_autor = $body['nombre_autor'];
			$acerca_autor_es = $body['acerca_autor_es'];
			$acerca_autor_en = $body['acerca_autor_en'];
			$acerca_autor_ch = $body['acerca_autor_ch'];
			$link_fb_a_n = $body['link_fb_a_n'];
			$link_tw_a_n = $body['link_tw_a_n'];
			$link_inst_a_n = $body['link_inst_a_n'];
			$img_autor = $body['img_autor'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createAutoresNoticias($nombre_autor, $acerca_autor_es, $acerca_autor_en, $acerca_autor_ch, $link_fb_a_n, $link_tw_a_n, $link_inst_a_n, $img_autor);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Insercion de Autor exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error insertando Autor",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Sacar valores-autores-noticias por Id
	  case "valores-autores-noticias-id":
		if(isset($_REQUEST)){
			$id_a_n = $_REQUEST['id_a_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_a_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_a_n = $body['id_a_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresAutoresNoticiasporId($id_a_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de valores de autores de noticias exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Autores de noticias",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  case "noticias":
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			
			$respuesta = $admin->valoresNoticias();
			
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Noticias exitosa",
						"data" => $respuesta						
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Noticias",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-noticias
	  case "crear-noticias":
		if(isset($_REQUEST)){
			$titulo_corto_n_es = $_REQUEST['titulo_corto_n_es'];
			$titulo_corto_n_en = $_REQUEST['titulo_corto_n_en'];
			$titulo_corto_n_ch = $_REQUEST['titulo_corto_n_ch'];
			$titulo_largo_n_es = $_REQUEST['titulo_largo_n_es'];
			$titulo_largo_n_en = $_REQUEST['titulo_largo_n_en'];
			$titulo_largo_n_ch = $_REQUEST['titulo_largo_n_ch'];
			$autor_n = $_REQUEST['autor_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['titulo_corto_n_es']) && isset($body['titulo_corto_n_en'])
			 && isset($body['titulo_corto_n_ch']) && isset($body['titulo_largo_n_es']) 
				 && isset($body['titulo_largo_n_en']) && isset($body['titulo_largo_n_ch'])
				  && isset($body['valor_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$titulo_corto_n_es = $body['titulo_corto_n_es'];
			$titulo_corto_n_en = $body['titulo_corto_n_en'];
			$titulo_corto_n_ch = $body['titulo_corto_n_ch'];
			$titulo_largo_n_es = $body['titulo_largo_n_es'];
			$titulo_largo_n_en = $body['titulo_largo_n_en'];
			$titulo_largo_n_ch = $body['titulo_largo_n_ch'];
			$autor_n = $body['autor_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createNoticia($titulo_corto_n_es, $titulo_corto_n_en, $titulo_corto_n_ch, $titulo_largo_n_es, 
												$titulo_largo_n_en, $titulo_largo_n_ch, $autor_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error insertando Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-noticias
	  case "noticias-categorias":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->noticiasCategorias($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Categroia de Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Consulta de Categroia de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-noticias
	  case "crear-noticias-categorias":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
			$id_c_n = $_REQUEST['id_c_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n']) && isset($body['id_c_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
			$id_c_n = $body['id_c_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createNoticiasCategorias($id_n, $id_c_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Categroia de Noticia Creada exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Creacion Categroia de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Sacar mostrar-imagenes-noticias por Id
	  case "mostrar-imagenes-noticias":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->valoresImagenesNoticiasporId($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de valores de autores de noticias exitosa",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error en consulta de Autores de noticias",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-imagen-noticias
	  case "crear-imagen-noticias":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
			$tipo_img_n = $_REQUEST['tipo_img_n'];
			$archivo_img = $_REQUEST['archivo_img'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n']) && isset($body['tipo_img_n']) && isset($body['archivo_img'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
			$tipo_img_n = $body['tipo_img_n'];
			$archivo_img = $body['archivo_img'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createImagenNoticias($id_n, $tipo_img_n, $archivo_img);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Imagen de Noticia Creada exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Creacion Imagen de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-descripcion-corta-noticias
	  case "crear-descripcion-corta-noticias":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
			$descripcion_corta_es = $_REQUEST['descripcion_corta_es'];
			$descripcion_corta_en = $_REQUEST['descripcion_corta_en'];
			$descripcion_corta_ch = $_REQUEST['descripcion_corta_ch'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n']) && isset($body['descripcion_corta_es']) && isset($body['descripcion_corta_en']) && isset($body['descripcion_corta_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
			$descripcion_corta_es = $body['descripcion_corta_es'];
			$descripcion_corta_en = $body['descripcion_corta_en'];
			$descripcion_corta_ch = $body['descripcion_corta_ch'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createdescripcionCortaNoticias($id_n, $descripcion_corta_es, $descripcion_corta_en, $descripcion_corta_ch);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Descripcion Corta de Noticia Creada exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Creacion Descripcion Corta de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-descripcion-larga-noticias
	  case "crear-descripcion-larga-noticias":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
			$descripcion_larga_es = $_REQUEST['descripcion_larga_es'];
			$descripcion_larga_en = $_REQUEST['descripcion_larga_en'];
			$descripcion_larga_ch = $_REQUEST['descripcion_larga_ch'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n']) && isset($body['descripcion_larga_es']) && isset($body['descripcion_larga_en']) && isset($body['descripcion_larga_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
			$descripcion_larga_es = $body['descripcion_larga_es'];
			$descripcion_larga_en = $body['descripcion_larga_en'];
			$descripcion_larga_ch = $body['descripcion_larga_ch'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->createdescripcionLargaNoticias($id_n, $descripcion_larga_es, $descripcion_larga_en, $descripcion_larga_ch);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Descripcion Larga de Noticia Creada exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Creacion Descripcion Larga de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	   //Ingresar crear-noticias-paises
	  case "crear-noticias-paises":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
			$codigo_pais = $_REQUEST['codigo_pais'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n']) && isset($body['codigo_pais'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
			$codigo_pais = $body['codigo_pais'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->crearNoticiasPaises($id_n, $codigo_pais);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Pais(es) de Noticia Creada exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Creacion Pais(es) de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar noticias-paises
	  case "noticias-paises":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->noticiasPaises($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Paises de Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Consulta de Paises de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Ingresar crear-noticias-tags
	  case "crear-noticias-tags":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
			$tags_noticias_es = $_REQUEST['tags_noticias_es'];
			$tags_noticias_en = $_REQUEST['tags_noticias_en'];
			$tags_noticias_ch = $_REQUEST['tags_noticias_ch'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n']) && isset($body['tags_noticias_es']) && isset($body['tags_noticias_en']) && isset($body['tags_noticias_ch'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
			$tags_noticias_es = $body['tags_noticias_es'];
			$tags_noticias_en = $body['tags_noticias_en'];
			$tags_noticias_ch = $body['tags_noticias_ch'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->crearNoticiasTags($id_n, $tags_noticias_es, $tags_noticias_en, $tags_noticias_ch);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Tags de Noticia Creada exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Creacion Tags de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Consultar consultar-noticias-id
	  case "consultar-noticias-id":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->consultarNoticiaId($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Consulta de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //Consultar consultar-noticias-id
	  case "consultar-descripcion-noticias-id":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->consultarDescripcionNoticiasId($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Descripcion de Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Consulta de Descripcion de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //consultar-descripcion-larga-noticias-id
	  case "consultar-descripcion-larga-noticias-id":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->consultarDescripcionLargaNoticiasId($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta de Descripcion Larga de Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Consulta de Descripcion Larga de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
	  //consultar-tags-noticias-id
	  case "consultar-tags-noticias-id":
		if(isset($_REQUEST)){
			$id_n = $_REQUEST['id_n'];
		}

		$body = json_decode(file_get_contents("php://input"), true);

		if(isset($body['id_n'])){
			$body = json_decode(file_get_contents("php://input"), true);
			$id_n = $body['id_n'];
		}
		if(isset($headers['Authorization']) && strlen($headers['Authorization']) > 0 && $headers['Authorization'] == AUTHORIZATION_ADMIN){
			$respuesta = $admin->consultarTagsNoticiasId($id_n);
			if($respuesta != false){
				echo json_encode([
						"status" => true,
						"message" => "Consulta Tags de Noticia exitoso",
						"data" => $respuesta
					  ]);
			}else{
				echo json_encode([
				"status" => false,
				"message" => "Error Consulta Tags de Noticia",				
				"data" => null
			  ]);
			}
		}else{		  
		  echo json_encode([
				"status" => false,
				"message" => "Error Consumo de Servicio",				
				"data" => null
			  ]);
		}
	  break;
  }
}
?>