<?php
extract($_GET);
extract($_POST);	
session_start();

include("lib/config.php");
$consecutivo = md5(date("Y-m-d H:i:s"));
if(isset($_SESSION["id_portal_inicio_sesion"]) && strlen($_SESSION["id_portal_inicio_sesion"])>0){
	header("Location: inicio/");
}else{
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    
<!-- Mirrored from themes.djavaui.com/blankon-fullpack-admin-theme/live-preview/admin/html/page-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 02 Jan 2018 16:40:13 GMT -->
<head>
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Sitio en internet más visitado por todos los que nos gusta enterarnos de las noticias y actualidad internacional completamente gratis.">
        <meta name="keywords" content="noticia, latinoamerica, colombia, suiza, internacional, actualidad, entretenimiento, deportes, tecnologia">
        <meta name="author" content="www.npm24.com">
		<meta name="google" content="notranslate" />
        <title>NPM24 - Ingresar</title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link rel="apple-touch-icon" sizes="114x114" href="/img/faviconlogo/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/faviconlogo/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/faviconlogo/favicon-16x16.png">
        <link rel="manifest" href="/img/faviconlogo/site.webmanifest">
        <link rel="mask-icon" href="/img/faviconlogo/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="/assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="/assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="/assets/admin/css/reset.css" rel="stylesheet">
        <link href="/assets/admin/css/layout.css" rel="stylesheet">
        <link href="/assets/admin/css/components.css" rel="stylesheet">
        <link href="/assets/admin/css/plugins.css" rel="stylesheet">
        <link href="/assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="/assets/admin/css/pages/sign.css" rel="stylesheet">
        <link href="/assets/admin/css/custom.css" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js"></script>
        <script src="./assets/global/plugins/bower_components/respond-minmax/dest/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
    </head>
    <!--/ END HEAD -->

    <!--

    |=========================================================================================================================|
	|  TABLE OF CONTENTS (Use search to find needed section)                                                                  |
	|=========================================================================================================================|
    |  01. @HEAD                        |  Container for all the head elements                                                |
	|  02. @META SECTION                |  The meta tag provides metadata about the HTML document                             |
	|  03. @FAVICONS                    |  Short for favorite icon, shortcut icon, website icon, tab icon or bookmark icon    |
	|  04. @FONT STYLES                 |  Font from google fonts                                                             |
	|  05. @GLOBAL MANDATORY STYLES     |  The main 3rd party plugins css file                                                |
	|  06. @PAGE LEVEL STYLES           |  Specific 3rd party plugins css file                                                |
	|  07. @THEME STYLES                |  The main theme css file                                                            |
	|  08. @IE SUPPORT                  |  IE support of HTML5 elements and media queries                                     |
	|=========================================================================================================================|
	|  09. @BODY                        |  Contains all the contents of an HTML document                                      |
	|  10. @WRAPPER                     |  Wrapping page section                                                              |
	|  11. @SIGN WRAPPER                |  Wrapping sign design                                                               |
	|=========================================================================================================================|
	|  12. @CORE PLUGINS                |  The main 3rd party plugins script file                                             |
	|  13. @PAGE LEVEL SCRIPTS          |  The main theme script file                                                         |
	|=========================================================================================================================|

    START @BODY
    |=========================================================================================================================|
	|  TABLE OF CONTENTS (Apply to body class)                                                                                |
	|=========================================================================================================================|
    |  01. page-boxed                   |  Page into the box is not full width screen                                         |
	|  02. page-sound                   |  For playing sounds on user actions and page events                                 |
	|=========================================================================================================================|

	-->
    <body class="page-sound">

        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @SIGN WRAPPER -->
        <div id="sign-wrapper">

            <!-- Brand -->
            <div class="brand">
                <img src="/img/logo.png" alt="brand logo"/>
            </div>
            <!--/ Brand -->

            <!-- Login form -->
            <div class="sign-in form-horizontal shadow rounded no-overflow" >
                <div class="sign-header">
                    <div class="form-group">
                        <div class="sign-text">
                            <span>NPM24 - Acceso de Usuarios Autorizados</span>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.sign-header -->
                <div class="sign-body">
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input type="text" class="form-control input-sm" placeholder="Usuario" name="username<?php echo $consecutivo;?>">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input type="password" class="form-control input-sm" placeholder="Contraseña" name="password<?php echo $consecutivo;?>">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.sign-body -->
                <div class="sign-footer">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="ckbox ckbox-theme">
                                    <input id="rememberme" type="checkbox">
                                    <label for="rememberme" class="rounded">Recordarme en este Equipo</label>
                                </div>
                            </div>
                            <div class="col-xs-6 text-right">
                                <!--<a href="page-lost-password.html" title="lost password">Perdio su Contraseña?</a>-->
                                <a href="#" title="lost password">Perdio su Contraseña?</a>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
						<div class="col-md-13">
                            <div class="alert alert-success" id="succes" style="display:none">
                                 <strong>Exito!</strong> Seras redireccionado al Panel de Aministraci&oacute;n.
                            </div>
							<div class="alert alert-warning" id="warning" style="display:none">
                                <strong>Precaucion!</strong> <span id="msg_warning">El campo usuario no puede estar vacio.</span>
                            </div>
							<div class="alert alert-danger" id="error" style="display:<?php if(isset($_GET["msg"]) && strlen($_GET["msg"])>0){?>block<?php }else{?>none<?php }?>">
                                <strong>Error!</strong> <?php if(isset($_GET["msg"]) && strlen($_GET["msg"])>0){ echo $_GET["msg"]; }else{?>Usuario o Contrase&nacute;a incorrectos.<?php }?>
                            </div>							
						</div>
						<div class="panel rounded shadow" id="loading" style="display:none;">
                                        <div class="panel-body" style="padding-left:45%;">
                                            <div>
                                                <img class="mr-15" data-no-retina src="./assets/global/img/loader/general/1.gif" alt="..."/>
                                            </div>
                                        </div><!-- /.panel-body -->
                                    </div>
                        <div style="width:100%;">
							<button class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Entrar</button>
						</div>
                    </div><!-- /.form-group -->
                </div><!-- /.sign-footer -->
            </div><!-- /.form-horizontal -->
            <!--/ Login form -->
			
            <!-- Content text -->
            <p class="text-muted text-center sign-link">Necesita una cuenta? <a href="#"> Solicítela Aquí</a></p>
            <!--/ Content text -->

        </div><!-- /#sign-wrapper -->
        <!--/ END SIGN WRAPPER -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
        <script src="assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
        <script src="assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
        <script src="assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="assets/admin/js/pages/blankon.sign.js"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->

        

    </body>
    <!-- END BODY -->
<script>
		$(function(){
				$("input:text[name='username<?php echo $consecutivo;?>']").on('keypress', function (event) {
					if(event.which === 13){
						auth_user();
					}
				 });
				 $("input:password[name='password<?php echo $consecutivo;?>']").on('keypress', function (event) {
					 if(event.which === 13){
						 auth_user();
					 }
				 });
				 $( "#login-btn" ).click(function() {
                    auth_user();
                 });
		});
		function auth_user(){
			$( "#loading" ).css("display", "block");
			$( "#succes" ).css("display", "none");
			$( "#warning" ).css("display", "none");
			$( "#error" ).css("display", "none");
			$("input:text[name='username<?php echo $consecutivo;?>']").prop('disabled', true);
			$("input:password[name='password<?php echo $consecutivo;?>']").prop('disabled', true);
			$( "#login-btn" ).prop('disabled', true);
			
			if($("input:text[name='username<?php echo $consecutivo;?>']").val() == ''){
				$( "#loading" ).css("display", "none");
				$( "#warning" ).css("display", "block");
				$( "#login-btn" ).prop('disabled', false);
				$( "#msg_warning" ).html("El campo nombre de usuario no puede estar vacio!");
				$("input:text[name='username<?php echo $consecutivo;?>']").focus();
				$("input:text[name='username<?php echo $consecutivo;?>']").prop('disabled', false);
				$("input:password[name='password<?php echo $consecutivo;?>']").prop('disabled', false);
				$( "#login-btn" ).prop('disabled', false);
				return false;
			}
			if($("input:password[name='password<?php echo $consecutivo;?>']").val() == ''){
				$( "#loading" ).css("display", "none");
				$( "#warning" ).css("display", "block");
				$( "#login-btn" ).prop('disabled', false);
				$( "#msg_warning" ).html("El campo Password no puede estar vacio!");
				$("input:password[name='password<?php echo $consecutivo;?>']").focus();
				$("input:text[name='username<?php echo $consecutivo;?>']").prop('disabled', false);
				$("input:password[name='password<?php echo $consecutivo;?>']").prop('disabled', false);
				$( "#login-btn" ).prop('disabled', false);
				return false;
			}
			if($("input:text[name='username<?php echo $consecutivo;?>']").val() != '' && $("input:password[name='password<?php echo $consecutivo;?>']").val() != ''){
				$.ajax(
                    {
                        type: "POST",
                        url: 'auth_inicio/',
                        data: { 'username<?php echo $consecutivo;?>':$("input:text[name='username<?php echo $consecutivo;?>']").val(), 'password<?php echo $consecutivo;?>':$("input:password[name='password<?php echo $consecutivo;?>']").val(), 'parametros':'<?php echo $consecutivo;?>' }, //intentando pasar textValue que deberia valer 10:10 pero me da el html de ipe.php
                        success: function(data){
                            $("#cargando_auth").css("display", "none");
                            var viene = JSON.parse(data);
							//console.log(data);
                            if(viene.status){
                                $("#loading").css("display", "block");
                               $( "#succes" ).css("display", "block");
                                setInterval(function(){ location.href = "/"; }, 1500);
                            }else if(!viene.status){
                                $("#loading").css("display", "none");
                                $( "#login-btn" ).prop('disabled', true);
                                $("input:text[name='username<?php echo $consecutivo;?>']").prop('disabled', false);
                                $("input:password[name='password<?php echo $consecutivo;?>']").prop('disabled', false);
                                $( "#error" ).css("display", "true");
								location.href = "/?msg="+viene.message;
                            }
                        }
                    });
			}
		}
</script>
</html>
<?php
}
?>